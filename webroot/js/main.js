

$(function(){
	$('.carousel').carousel({
	  interval: 3000
	});

	$('.carouselControl').on('click',function(e){
		e.preventDefault();
		$('.carousel').carousel($(this).data('direction'));
		return false;
	});
});