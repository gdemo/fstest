// plugin pattern from https://css-tricks.com/snippets/jquery/jquery-plugin-template/
(function($){
    $.FSloader = function(el, options){
        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var base = this;
        
        // Access to jQuery and DOM versions of element
        base.$el = $(el);
        base.el = el;
        
        // Add a reverse reference to the DOM object
        base.$el.data("FSloader", base);
        base.findQuery = {};

        $.FSloader.defaultOptions = {
            htmlPatternListItem: ".pattern",
            outputRenderList: ".renderList",
            pageLoader: ".loader",
            loadMoreButton: ".loadMore",
            stragePlugin: false,
            categoryMenu: "#categoryMenu",
            citySelect: "select.byCity",
            activeCategoryMenuClass: "active",
            defaultLoadLocation: "Amsterdam"
        };
        
        base.initAppSetting = function(){
            base.htmlPattern = base.$el.find(base.options.htmlPatternListItem).html();
            base.appUrl = base.$el.data('appUrl');
            base.renderList = base.$el.find(base.options.outputRenderList);
            base.$loaderEl = base.$el.find(base.options.pageLoader);  
            base.$loadMore = base.$el.find(base.options.loadMoreButton);  
            base.$citySelect = base.$el.find(base.options.citySelect);  
            base.$categoryMenu = $(base.options.categoryMenu);  
            
            base.findQuery = {
                near: '',
                venuePhotos: 1,
                section: '',
                offset: 0,
                limit: 6
            };            
            
        };

        base.bindCateogryMenu = function(){

            base.$categoryMenu.find('a').on('click',function(e){
                e.preventDefault();

                base.$categoryMenu.find('li').removeClass(base.options.activeCategoryMenuClass);
                $(this).parent('li').addClass(base.options.activeCategoryMenuClass);

                base.clearOutput = true;
                base.changeParams();

                return false;
            });
        };

        base.getCategoryParam = function(){
            var category = base.$categoryMenu.find('li.'+base.options.activeCategoryMenuClass+' a').data('category');
            return category;
        }

        base.init = function(){
            
            base.options = $.extend({},$.FSloader.defaultOptions, options);
            base.initAppSetting();
            base.bindSelect();
            base.bindCateogryMenu();
            base.loadByParams();
            base.paginate();

        };
        
        base.paginate = function(){
            base.$loadMore.on('click' , function(e){
                e.preventDefault();
                base.loader.start();
                base.findQuery.offset += base.findQuery.limit;
                base.clearOutput = false;
                base.loadData(base.render);
                return false;
            });
        };

        base.bindSelect = function(){
            base.$citySelect.on('change',base.changeParams);
        };

        base.changeParams = function(){
            base.findQuery.offset = 0;
            base.loadByParams();
        };

        base.loadByParams = function(){
            base.getCategoryParam();
            base.loader.start();
            base.findQuery.near = base.$citySelect.val();
            base.findQuery.section = base.getCategoryParam();
            base.clearOutput = true;
            base.loadData(base.render);
        };


        base.loadData = function(callback){

            base.findUrlQueryString = jQuery.param( base.findQuery );

            // if exist storage manager 

            if( typeof base.options.stragePlugin == 'object' ){

                base.cacheData = base.options.stragePlugin.get(base.findUrlQueryString);

                if( base.cacheData != null ){
                    callback(base.cacheData);
                    return ;
                }

            }

            $.getJSON( base.appUrl+"api/index/explore/"+base.findUrlQueryString, function( data ) {

                if( typeof base.options.stragePlugin == 'object' ){
                    base.options.stragePlugin.set( base.findUrlQueryString , data );
                }

                callback(data); 
            });
        };

        base.loader = {
            start : function(){
                base.$loaderEl.removeClass('hide');
            },

            stop : function(){
                base.$loaderEl.addClass('hide');
            }
        };

        base.render = function(data){

              var items = [];

              if(base.clearOutput){
                base.renderList.html('');
              }

              $.each( data, function( key, val ) {

                    var tmps = base.htmlPattern;

                    $.each(val, function(i,v) {
                        tmps = tmps.replace('{{'+i+'}}',v);
                    });

                    items.push(tmps);
                
              });

             
              $( "<div>", {
                "class": "my-new-list",
                html: items.join( "" )
              }).appendTo(base.renderList);

              base.loader.stop();

        };

        // Run initializer
        base.init();
    };
    

    
    $.fn.FSloader = function(options){
        return this.each(function(){
            (new $.FSloader(this, options));
        });
    };
    
})(jQuery);

// include $.jStorage plugin http://www.jstorage.info/
$(function(){
    $('.FSloader').FSloader({
        stragePlugin: $.jStorage
    });
});
