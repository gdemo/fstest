<?php
namespace App\Controller;

use App\Controller\AppController;
use Jcroll\FoursquareApiClient\Client\FoursquareClient;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use App\Model\Entity\Foursquare;

class ApiController extends AppController
{

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->autoRender = false;
        $this->Foursquare = new Foursquare();     
    }

    // using https://github.com/jcroll/foursquare-api-client 
    
    public function index($type,$params){
        
        parse_str($params , $get_array);
        
        $results = $this->Foursquare->getVentures($type,$get_array);

        echo json_encode($this->Foursquare->parseVentures($results));

    }
    
}