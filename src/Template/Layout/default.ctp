<?php
$cakeDescription = 'Test site';
?>
<!DOCTYPE html>
<html>
<head>
	<?= $this->Html->charset() ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">    
	<title>
		<?= $cakeDescription ?>:
		<?= $this->fetch('title') ?>
	</title>
	<?= $this->Html->meta('icon') ?>


	<?php echo $this->Html->css('/css/Bootstrap/css/bootstrap.min'); ?>
	<?php echo $this->Html->css('/css/Bootstrap/css/bootstrap.theme'); ?>
	<?php echo $this->Html->css('/css/Bootstrap/css/bootstrap.theme.responsive'); ?>
	<?php echo $this->Html->css('/css/FSloader/style'); ?>
	<?php echo $this->Html->css('/css/font-awesome.min'); ?>
	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>
	<?= $this->fetch('script') ?>


	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->    
	
</head>
<body>
   
	<header>
		<nav class="navbar navbar-default ">
			<div class="container">
					<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" title="back to home" href="#"><img src="./img/logo.png" alt="site logo" /></a>
					</div>

					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right" id="categoryMenu">
									<li class="active"><a data-category="coffee" href="#">Coffee</a></li>
									<li><a data-category="drinks" href="#">Drinks</a></li>
									<li><a data-category="shops" href="#">Shops</a></li>
									<li><a data-category="outdoors" href="#">Outdoors</a></li>
							</ul>
					</div>
			</div>
		</nav>
	</header>


	<?php echo $this->fetch('content'); ?>
	

	<footer>
			<div class="container ">
					
				<div class="footerCover">
					<img src="img/footer-logo.png" alt="signature img description" class="pull-left" /> 
					<p>
							&copy; 2015 test-site.com 
					</p>

					<div class="footerPlaceholder visible-xs"></div>
				</div>

			</div>
	</footer>

	<div class="backToTopButtonCover visible-xs">
		<div class="container">
			<a class="btn btn-primary btn-lg btn-block" href="#">Back to top</a>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	<?php echo $this->Html->script('Bootstrap/js/bootstrap.min'); ?>                
	<?php echo $this->Html->script('jstorage/jstorage'); ?>                
	<?php echo $this->Html->script('FsLoader'); ?>                
	<?php echo $this->Html->script('main'); ?>                

</body>
</html>
