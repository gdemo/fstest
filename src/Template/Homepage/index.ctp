	<section>
			<div class="carouselContainer">
				<div id="carousel-example-generic" class="carousel container slide" >


						<!-- Wrapper for slides -->
						<div class="carousel-inner " role="listbox">

								<div class="item active ">
										<div class="row">
												<div class="col-lg-8 col-md-8">
														<img src="img/c1.jpg" alt="...">							      
												</div>
												<div class="col-lg-4 col-md-4">
														<div class="innerText">
															<div class="date pull-left">22.12.2015</div>
															<div class="rating pull-right">152</div>
															<h2 class="clearfix">Dodge charger 1969</h2>
															<div class="company">Compaby: <strong>Porshe</strong></div>
															<p>
																Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempor, enim non cursus euismod, enim nisi scelerisque neque, eu pretium felis orci non ante. Nulla at lacinia nisl. Etiam eu molestie odio, eu pellentesque eros. Donec ut turpis a urna aliquet posuere
															</p>
															<a href="#" class="pull-right btn">Visit page <i class="fa fa-caret-right"></i></a>
															<div class="clearfix"></div>
														</div>
												</div>
										</div>
								</div>

								<div class="item">
										<div class="row">
												<div class="col-lg-8 col-md-8">
														<img src="img/c2.jpg" alt="...">                                
												</div>
												<div class="col-lg-4 col-md-4">
														<div class="innerText">
															<div class="date pull-left">22.12.2015</div>
															<div class="rating pull-right">152</div>
															<h2 class="clearfix">Ferrari testa rossa GT250</h2>
															<div class="company">Compaby: <strong>Porshe</strong></div>
															<p>
																Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempor, enim non cursus euismod, enim nisi scelerisque neque, eu pretium felis orci non ante. Nulla at lacinia nisl. Etiam eu molestie odio, eu pellentesque eros. Donec ut turpis a urna aliquet posuere
															</p>
															<a href="#" class="pull-right btn">Visit page <i class="fa fa-caret-right"></i></a>
															<div class="clearfix"></div>
														</div>
												</div>
										</div>
								</div>

						</div>


				</div>

			</div>

			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8">
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="carouselControles">
							<div class="btn-group btn-group-justified" role="group" aria-label="...">
							  <a data-direction="prev" class="btn btn-primary carouselControl"><i class="fa fa-chevron-left"></i> Prev</a>
							  <a data-direction="next" class="btn btn-primary carouselControl">next <i class="fa fa-chevron-right"></i></a>
							</div>
						</div>
					</div>
				</div>                
			</div>

	</section>

	<div class="container articleList">
		<div class="row">
			<section data-app-url="<?php echo $appUrl ?>" class="FSloader">
				<div class="loader hide">Loading...</div>
				<div class="col-sm-12 col-md-12">
					<form class="form-inline">
					  <div class="form-group">
						<label for="exampleInputName2">Select city</label>
						<select id="exampleInputName2" class="form-control byCity">
							<option value="Prague">Prague</option>
							<option value="London">London</option>
							<option value="Brno">Brno</option>
							<option value="Amsterdam" selected="selected">Amsterdam</option>
						</select>  
					  </div>
					</form>  
					<br />
				</div>
				<div class="renderList"></div>
				<div class="pattern">
					<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
									<div class="pageRating">{{rating}}</div>
									<img src="{{img}}" alt="...">
									<div class="caption">
											<h3><a target="_blank" href="https://foursquare.com/v/{{id}}" titile="">{{name}}</a></h3>
											<div class="btn-group btn-group-justified" role="group" aria-label="...">
													<a class="btn btn-default"><i class="fa fa-twitter"></i></a>
													<a class="btn btn-default"><i class="fa fa-facebook"></i></a>
													<a class="btn btn-default"><i class="fa fa-smile-o"></i></a>
											</div>
											<p class="author">
													Author: <a href="#" title="...">{{user_name}}</a>
											</p>
											<p class="textInner">
												{{text}}
											</p>

									</div>
							</div>
					</div>              
				</div>
				
				<div class="col-sm-12 col-md-12 center">
					<a class="btn btn-primary loadMore btn-lg">Load more</a>
				</div>
				
			</section>            
		</div>
	</div>