<?php

namespace App\Model\Entity;

use Jcroll\FoursquareApiClient\Client\FoursquareClient;
use Cake\Core\Configure;
use Cake\ORM\Entity;
use Cake\Utility\Text;
use Cake\Cache\Cache;

class Foursquare extends Entity
{

	protected $FoursquareClient ;
	protected $FScommand;
	protected $cacheQueryHash;
	protected $cacheResult;

	public function getVentures($type,$params){

		$this->cacheQueryHash = md5($type.serialize($params));

		$this->cacheResult = Cache::read($this->cacheQueryHash, 'short');

		if(!$this->cacheResult){

			$this->FoursquareClient = FoursquareClient::factory(Configure::read('FoursquareApi'));   

			$this->FScommand = $this->FoursquareClient->getCommand('venues/'.$type, $params );
			
			$this->cacheResult = $this->FScommand->execute(); 

			Cache::write($this->cacheQueryHash, $this->cacheResult , 'short');

		}

		return $this->cacheResult;

	}


	// hard coding :) in ideal world I would use structure saved in database for select what need from response structure
	// for minimum data in client site becose in the client save data to local storage

	public function parseVentures($fsResponse){

		$data = $fsResponse['response']['groups'][0]['items'];

		$output = [];
		
		foreach ( $data AS $venue ){
			$tmp = [];
			
			$tmp['name'] = $venue['venue']['name'];
			$tmp['id'] = $venue['venue']['id'];
			$tmp['rating'] = $venue['venue']['rating'];
			// problem with ssl connection for loading images on frontend
			// $tmp['img'] = $venue['venue']['photos']['groups'][0]['items'][0]['prefix'].'600x400'.$venue['venue']['photos']['groups'][0]['items'][0]['suffix'];
			$tmp['img'] = 'http://irs0.4sqi.net/img/general/'.'600x400'.$venue['venue']['photos']['groups'][0]['items'][0]['suffix'];
			$tmp['text'] = Text::truncate($venue['tips'][0]['text'],150);
			$tmp['user_name'] = $venue['tips'][0]['user']['firstName'];
			$tmp['user_id'] = $venue['tips'][0]['user']['id'];
			
			$output[] = $tmp;
		}

		return $output;		
	}

}