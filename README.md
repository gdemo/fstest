# Simple test site

Sample project 

## For programers

1. Download [Composer](http://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar install`.

If Composer is installed globally, run
```bash
composer install
```

## Configuration

Set Yousquare api key in 
`config/bootstrap.php - line:211,212`

## For Managers :)
[Online demo site](http://test2.ok-reality.sk)

### Using technology
- [JQuery](https://jquery.com/) Front-end site programming
- [foursquare-api-client](https://github.com/jcroll/foursquare-api-client)
- [JSotage](http://www.jstorage.info/) Local storage JQuery plugin
- [CakePHP](http://cakephp.org/) For backend api site and project structure

## My work 

- `src/Controller/ApiController.php` - example php programming
- `src/Model/Entity/Foursquare.php`
- `src/Template/Homepage/index.ctp`
- `src/Template/Homepage/index.ctp`
- `src/Template/Layout/default.ctp`
- `webroot/js/FsLoader.js` - example js/jquery plugin
- `webroot/css/Bootstrap/css/bootstrap.theme.css` - example css 